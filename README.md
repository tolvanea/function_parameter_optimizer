#function_parameter_optimizer

Alpi Tolvanen

Licence: MIT or just Public Domain

## About
This library implements Gauss-Newton and Levenberg-Marquardt methods. The fit function parameter to measured noisy data by minimizing least squares sum.

This project is part of Tampere University course
MAT-60456 Optimization methods

## Content
### Library `function_parameter_optimizer`
There are 4 functions in `optimization_algorithms.rs`

* `gauss_newton`
* `gauss_newton_with_autodiff`
* `levenberg_marquardt`
* `levenberg_marquardt_with_autodiff`

Postfix `_with_autodiff` means that calculation of gradient is made automatically, if provided function has replaced `f64`-type with `peroxide::Number` from automatic differentiation library `peroxide`. The most recommended optimization algorithms are `levenberg_marquardt_with_autodiff` or `levenberg_marquardt` as they are the most robust. However, they too converge to local minimum.

Check tests for example usage.

### Binary `optibize_bin`
This binary fits parameters for few example functions and plots the results.

## Depedencies
Python matplotlib is required for binary. 