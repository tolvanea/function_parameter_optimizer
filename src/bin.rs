use ndarray as nd;
#[allow(unused_imports)]
use nd::prelude::{Dimension, ShapeBuilder, AsArray, Array1, Array2, Axis, arr1, arr2, Ix1, Ix2};
use std::path::Path;
use std::fs;
//use ndarray_rand::RandomExt;
//use ndarray_rand::rand_distr::Normal;
//use peroxide as pr;
//use pr::{Number};

use function_parameter_optimizer::opmization_algorithms::{
    OptimizeError, gauss_newton, levenberg_marquardt_with_autodiff};
use function_parameter_optimizer::model_functions::{poly, residual, gaussian, high_oscillation};
use peroxide::{Plot2D, Plot, Point, Line};

fn plot_1d_function_fit<F>(
    xgrid: Array2<f64>,
    data: Array1<f64>,
    f:F,                            // Optimizable function
    p_real: Option<Array1<f64>>,    // Parameters of original underlying fit
    p1: Option<Array1<f64>>,        // Parameters of fit 1
    p2: Option<Array1<f64>>,        // Parameters of fit 2
    label1: Option<&str>,           // Label of fit 1
    label2: Option<&str>,           // Label of fit 2
    name: &str,                     // Label name and save name
)
where F: Fn(&[f64], &[f64]) -> f64 + Copy
{
    let mut plt = Plot2D::new();
    plt.set_domain(xgrid.clone().into_raw_vec())
        .insert_image(data.clone().into_raw_vec());
    let mut markers = vec![Point];
    let mut labels = vec!["Data"];
    let label;

    // Optional curves to be drawn
    if let Some(p_real) = p_real {
        let fit_orig: Vec<f64> = xgrid.outer_iter()
            .map(|x| f(x.as_slice().unwrap(), p_real.as_slice().unwrap()))
            .collect();
        plt.insert_image(fit_orig);
        label = format!("Hidden model, residual={:.2}", residual(&xgrid, &data, f, &p_real));
        labels.push(label.as_str());
        markers.push(Line);
    }
    if let Some(p1) = p1 {
        let fit1: Vec<f64> = xgrid.outer_iter()
            .map(|x| f(x.as_slice().unwrap(), p1.as_slice().unwrap()))
            .collect();
        plt.insert_image(fit1);
        labels.push(label1.unwrap_or(""));
        markers.push(Line);
    }
    if let Some(p2) = p2 {
        let fit2: Vec<f64> = xgrid.outer_iter()
            .map(|x| f(x.as_slice().unwrap(), p2.as_slice().unwrap()))
            .collect();
        plt.insert_image(fit2);
        labels.push(label2.unwrap_or(""));
        markers.push(Line);
    }

    // create directory if it not exist
    if !Path::new("output_figures").is_dir() {
        fs::create_dir("output_figures").unwrap();
    }

    plt.set_xlabel("$x$")
        .set_ylabel("$f(x)$")
        .set_legend(labels)
        .set_title(name)
        .set_path(&format!("output_figures/{}.pdf", name))
        .set_marker(markers)
        .set_fig_size((5, 4))
        .savefig()
        .expect("Can't draw a plot");
}


/// Test 8th degree polynomial.
/// This is linear problem, but my algorithm solves it somewhy in two steps.
fn plot_8th_degree_polynomial() {
    // polynomial terms of cosine
    let p_real: Array1<f64> = arr1(&[1./40320., 0., 1./720.0, 0., 1./24., 0., -0.5, 0., 1.]);
    let p_0: Array1<f64> = Array1::from_elem(p_real.len(), 1.);
    let (xgrid, data, min_err) = poly::data(p_real.as_slice().unwrap());
    let res = gauss_newton(poly::with_f64, poly::grad_p, &xgrid, &data, &p_0, 1000, 1e-6);
    let (p1, itrs1) = match res {
        Ok(v) => v,
        Err(OptimizeError::NonConvexFunction(p)) => {
            println!("This darn nonconvex error again, crap!");
            (p, 0)
        },
        Err(e) => panic!(e),
    };
    let (p2, itrs2) = levenberg_marquardt_with_autodiff(poly::with_num, &xgrid, &data, &p_0).unwrap();
    let err1 = residual(&xgrid, &data, poly::with_f64, &p1);
    let err2 = residual(&xgrid, &data, poly::with_f64, &p2);

    plot_1d_function_fit(
        xgrid.clone(),
        data.clone(),
        poly::with_f64,
        Some(p_real.clone()),
        Some(p1.clone()),
        Some(p2.clone()),
        Some(&format!("GN, residual={:.2}, iters={}", err1, itrs1)),
        Some(&format!("LM, residual={:.2}, iters={}", err2, itrs2)),
        "8th order polynomial",
    );

    println!("\n8th degree polynomial");
    println!("                      Real parameters:   {}", p_real);
    println!("Gauss Newton:         Fitted parameters: {} iterations: {}", p1, itrs1);
    println!("Levenberg-Marquardt:  Fitted parameters: {} iterations: {}", p2, itrs2);
    assert!(err1 / min_err < 1.01, "err: {}", err1);
    assert!(err2 / min_err < 1.01, "err: {}", err2);
}


/// Gaussian curve fitting.
/// This is not linear problem, so more than one step is required
/// Also Gauss-Newton does not work here, as problem is a bit nasty.
fn plot_gaussian() {
    let (xgrid, data, p_real, min_err) = gaussian::data();
    let p_0: Array1<f64> = Array1::from_elem(p_real.len(), 1.);
    let (p2, itrs2) = levenberg_marquardt_with_autodiff(gaussian::f, &xgrid, &data, &p_0).unwrap();
    let err2 = residual(&xgrid, &data, gaussian::f, &p2);

    plot_1d_function_fit(
        xgrid.clone(),
        data.clone(),
        gaussian::f,
        Some(p_real.clone()),
        Some(p2.clone()),
        None,
        Some(&format!("LM, residual={:.2}, iters={}", err2, itrs2)),
        None,
        "Gaussian"
    );

    println!("\nGaussian");
    println!("                     Real parameters:   {}", p_real);
    println!("Gauss Newton:        Just blows");
    println!("Levenberg-Marquardt: Fitted parameters: {} iterations: {}", p2, itrs2);
    assert!(err2 / min_err < 1.01, "err: {}", err2);
}


/// Test 8th degree polynomial.
/// This is linear problem, but my algorithm solves it somewhy in two steps.
fn plot_high_oscillations() {
    let p_slice = &[2.0, 4.0];
    let p_real: Array1<f64> = arr1(p_slice);
    let (xgrid, data, min_err) = high_oscillation::data(p_slice);
    let p_0: Array1<f64> = Array1::from_elem(p_real.len(), 1.);
    let (p2, itrs2) =
        levenberg_marquardt_with_autodiff(high_oscillation::with_num, &xgrid, &data, &p_0).unwrap();
    let err2 = residual(&xgrid, &data, high_oscillation::with_f64, &p2);

    plot_1d_function_fit(
        xgrid.clone(),
        data.clone(),
        high_oscillation::with_f64,
        Some(p_real.clone()),
        Some(p2.clone()),
        None,
        Some(&format!("LM, residual={:.2}, iters={}", err2, itrs2)),
        None,
        "$f(x) = a \\cos(bx) + b \\sin(ax)$"
    );

    println!("\nPlot high oscillation");
    println!("                     Real parameters:   {}", p_real);
    println!("Gauss Newton:        Just blows");
    println!("Levenberg-Marquardt: Fitted parameters: {} iterations: {}", p2, itrs2);
    if !(err2 / min_err < 1.01) {
        println!("Levenberg-Marquardt has not found optimal parameters.")
    }
}

fn main() {
    plot_8th_degree_polynomial();
    plot_gaussian();
    plot_high_oscillations();
}
