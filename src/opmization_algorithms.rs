use ndarray as nd;
#[allow(unused_imports)]  // Copy paste these imports on every file
use nd::{Dimension, ShapeBuilder, AsArray, ScalarOperand,
         ArrayBase, Array, Array1, Array2, ArrayView1, ArrayView2,
         arr1, arr2, Axis, Ix1, Ix2, Data, OwnedRepr, Dim};
#[allow(unused_imports)]
use anyhow::{Context, Result as Res, anyhow};
use thiserror::Error;
use peroxide as pr;
use pr::{Real, Number};

use super::misc::{inv, pseudo_inv, norm, square_sum, gradient};

const ITER_COUNT: usize = 10000; // Default iteration cap for algorithm
const THRESHOLD: f64 = 1e-6;    // Default threshold that determines convergence


/// Error type that is returned from solvers. All variants contain the most
/// recent estimation for parameters before exiting.
#[derive(Error, Debug)]
pub enum OptimizeError {
    /// Function is not convex
    #[error("Function is not convex with parameters: `{0:?}`")]
    NonConvexFunction(Array1<f64>),
    /// Curvature of function is almost singular
    #[error("Curvature of function is almost singular with parameters: `{0:?}`")]
    AlmostSingularFunction(Array1<f64>),
    /// Solver got stuck. Probable cause: function may not be differentiable
    #[error("Solver got stuck. Is your function differentiable? Parameters: {0:?}")]
    GotStuck(Array1<f64>),
    /// Function returned NaN
    #[error("Function returned NaN for some x with parameters: `{0:?}`")]
    FunctionReturnedNan(Array1<f64>),
    // Iteration maximum reached
    #[error("Iteration maximum reached. Parameters: `{0:?}`")]
    IterationMaximumReached(Array1<f64>),
}



/// Gauss newton method with automatic differentiation (ie. you do not need
/// calculate gradient of a function yourself)
/// https://en.wikipedia.org/wiki/Gauss%E2%80%93Newton_algorithm
///
/// Function may find saddle point as a result, `OptimizeError::NonConvexFunction`
/// is returned only when function is strongly non-convex. Please use more robust
/// Levenberg–Marquardt algorithm if you are not sure if function is convex.
///
/// # Arguments
///
/// * `f(r: &[f64], p: &[Number])` - Function that uses `peroxide::Number` in
///     arithmetics instead of f64. This makes automatic differentiation possible.
///
///     * `r` - Evaluation point. With 1d functions this is slice with length 1.
///     * `p` - Parameters that are optimized. Note: type is `peroxide::Number`.
///
/// * `xgrid` - 2d ndarray with rows being evaluation points.
/// * `data` - 1d ndarray of data that will be fitted against. Make sure that
///     `data.len() == xgrid.dim().0`
/// * `p_init` - 1d ndarray of initial parameters
///
/// # Return
/// Locally optimal parameters and iteration count
///
/// # Errors
///
/// * `OptimizeError::NonConvexFunction`
/// * `OptimizeError::AlmostSingularFunction`
/// * `OptimizeError::FunctionReturnedNan`
/// * `OptimizeError::IterationMaximumReached`
///
pub fn gauss_newton_with_autodiff<F, D>(
    f: F,
    xgrid: &ArrayBase<D, Ix2>,
    data: &ArrayBase<D, Ix1>,
    p_init: &ArrayBase<D, Ix1>, // Parameters
) -> Result<(Array1<f64>, usize), OptimizeError>
where
    D: Data<Elem=f64>,
    F: Fn(&[f64], &[Number]) -> Number,
{
    let grad = |r: &[f64], p: &[f64]| gradient(|p| f(r, p), p);
    let f_f64 = |r: &[f64], p: &[f64]|
        f(r, p.iter().map(|&x| Number::F(x)).collect::<Vec<Number>>().as_slice()).to_f64();
    return gauss_newton(f_f64, grad, xgrid, data, p_init, ITER_COUNT, THRESHOLD);
}

/// Gauss newton method, but you need to calculate manually the gradient of the
/// function. Recommended to use easier `gauss_newton_with_autodiff` over this.
/// However this funciton is handy if `peroxide::Number` is confusing.
///
/// Convergence is achieved if step is small and error does not change.
/// See `gauss_newton_with_autodiff` for better documentation.
pub fn gauss_newton<F1, F2, D>(
    f: F1,
    gradient: F2,
    xgrid: &ArrayBase<D, Ix2>,
    data: &ArrayBase<D, Ix1>,
    p_init: &ArrayBase<D, Ix1>, // Parameters
    iters: usize,               // Iteration count
    threshold: f64,             // Convergence threshold
) -> Result<(Array1<f64>, usize), OptimizeError>
where
    D: Data<Elem=f64>,
    F1: Fn(&[f64], &[f64]) -> f64,
    F2: Fn(&[f64], &[f64]) -> Array1<f64>,
{
    assert_eq!(
        xgrid.dim().0,
        data.len(),
        "xgrid must constist of row vectors of measurement points"
    );
    assert_eq!(
        {
            let p = p_init.as_slice().unwrap();
            let x = xgrid.index_axis(Axis(0),1);
            gradient(x.as_slice().unwrap(), p).len()
        },
        p_init.len(),
        "Gradient must with respect of parameters. (So gradient(x,p).len() == p_init.len()"
    );
    assert!(iters > 0);
    assert!(threshold > 0.0);
    let mut p = p_init.to_owned();
    // Jacobian
    let mut jac = Array2::zeros((data.dim(), p_init.len()));  // Jacobian
    let mut res = Array1::zeros(data.dim());  // residuals
    let mut iter = 0;
    let mut err_old = std::f64::MAX;
    let mut err_new;
    loop {
        for (mut jac_row, r ) in jac.outer_iter_mut().zip(xgrid.outer_iter()) {
            jac_row.assign(&gradient(r.as_slice().unwrap(), p.as_slice().unwrap()));
        }
        for ((r, v ), x) in res.iter_mut().zip(data.iter()).zip(xgrid.outer_iter()) {
            *r = v - f(x.as_slice().unwrap(), p.as_slice().unwrap());
        }
        if res.iter().any(|x| x.is_nan()) {
            return Err(OptimizeError::FunctionReturnedNan(p));
        }

        let pinv = match pseudo_inv(jac.clone()){
            Ok(v) => v,
            Err(_) => return Err(OptimizeError::AlmostSingularFunction(p)),
        };
        let step = pinv.dot(&res);

        err_new = square_sum(&res);
        // Slightly concave functions are allowed
        if err_new > err_old*1.0001 {
            return Err(OptimizeError::NonConvexFunction(p));
        }
        if (norm(&step) < threshold) && (err_new - err_old).abs() < threshold {
            break;
        }
        err_old = err_new;
        p = p + &step;
        iter += 1;
        if iter > iters {
            return Err(OptimizeError::IterationMaximumReached(p));
        }
    }
    return Ok((p, iter));
}



/// Levenberg–Marquardt method to solve optimization problem. This is *the*
/// function parameter optimizer you are looking for.
/// https://en.wikipedia.org/wiki/Gauss%E2%80%93Newton_algorithm
///
/// Automatic differentiation is used so you do not need calculate gradient of a
/// function yourself. If you do not undestand how peroxide::Number works, you
/// can always use the raw implementation `levenberg_marquardt`
///
/// # Returns
/// Locally optimal parameters and iteration count
///
/// # Arguments
///
/// * `f(r: &[f64], p: &[Number])` - Function that uses `peroxide::Number` in
///     arithmetics instead of f64. This makes automatic differentiation possible.
///     If you do not understand how to construct such function, you can always
///     use `levenberg_marquardt`.
///
///     * `r` - Evaluation point. With 1d functions this is slice with length 1.
///     * `p` - Parameters that are optimized. Note: type is `peroxide::Number`.
///
/// * `xgrid` - 2d ndarray with rows being evaluation points.
/// * `data` - 1d ndarray of data that will be fitted against. Make sure that
///     `data.len() == xgrid.dim().0`
/// * `p_init` - 1d ndarray of initial guess for parameters
///
/// # Errors
///
/// * `OptimizeError::FunctionReturnedNan`
/// * `OptimizeError::IterationMaximumReached`
/// * `OptimizeError::GotStuck`
pub fn levenberg_marquardt_with_autodiff<F, D>(
    f: F,
    xgrid: &ArrayBase<D, Ix2>,  // Data points
    data: &ArrayBase<D, Ix1>,   // Measurement results at those points
    p_init: &ArrayBase<D, Ix1>, // Initial guess for parameters
) -> Result<(Array1<f64>, usize), OptimizeError>
where
    D: Data<Elem=f64>,
    F: Fn(&[f64], &[Number]) -> Number,
{
    let grad = |r: &[f64], p: &[f64]| gradient(|p| f(r, p), p);
    let f_f64 = |r: &[f64], p: &[f64]|
        f(r, p.iter().map(|&x| Number::F(x)).collect::<Vec<Number>>().as_slice()).to_f64();
    return levenberg_marquardt(f_f64, grad, xgrid, data, p_init, ITER_COUNT, THRESHOLD);
}

/// Raw version of Levenberg–Marquardt method, but now you need to calculate manually
/// the gradient. Recommended to use easier `levenberg_marquardt_with_autodiff`
/// over this. However this function is handy if `peroxide::Number` is confusing.
///
/// Convergence achieved when step is small and residual change is small.
/// See `levenberg_marquardt_with_autodiff` for better documentation.
///
/// Variable naming is used after wikipedia:
/// https://en.wikipedia.org/wiki/Levenberg%E2%80%93Marquardt_algorithm
/// Good paper with better description of LM-algorithm:
/// http://www.nnw.cz/doi/2011/NNW.2011.21.020.pdf
/// Good slides
/// https://madsc.lanl.gov/presentations/Leif_LM_presentation_m.pdf
pub fn levenberg_marquardt<F1, F2, D>(
    f: F1,
    gradient: F2,
    xgrid: &ArrayBase<D, Ix2>,
    data: &ArrayBase<D, Ix1>,
    p_init: &ArrayBase<D, Ix1>, // Initial guess for parameters
    iters: usize,               // Iteration count
    threshold: f64,             // Convergence threshold
) -> Result<(Array1<f64>, usize), OptimizeError>
where
    D: Data<Elem=f64>,
    F1: Fn(&[f64], &[f64]) -> f64,
    F2: Fn(&[f64], &[f64]) -> Array1<f64>,
{
    assert_eq!(
        xgrid.dim().0,
        data.len(),
        "xgrid must constist of row vectors of measurement points"
    );
    assert_eq!(
        {
            let p = p_init.as_slice().unwrap();
            let x = xgrid.index_axis(Axis(0),1);
            gradient(x.as_slice().unwrap(), p).len()
        },
        p_init.len(),
        "Gradient must with respect of parameters. (So gradient(x,p).len() == p_init.len()"
    );
    assert!(iters > 0);
    assert!(threshold > 0.0);

    let calc_residual = |p: &Array1<f64>, res: &mut Array1<f64>| {
        for ((r, v ), x) in res.iter_mut().zip(data.iter()).zip(xgrid.outer_iter()) {
            *r = v - f(x.as_slice().unwrap(), p.as_slice().unwrap());
        }
    };
    let calc_jacobian = |p: &Array1<f64>, jac: &mut Array2<f64>| {
        for (mut jac_row, r ) in jac.outer_iter_mut().zip(xgrid.outer_iter()) {
            jac_row.assign(&gradient(r.as_slice().unwrap(), p.as_slice().unwrap()));
        }
    };

    let mut p = p_init.to_owned();
    // Jacobian
    let mut jac = Array2::zeros((data.dim(), p_init.len()));  // Jacobian
    let mut res = Array1::zeros(data.dim());  // residuals
    calc_residual(&p, &mut res);
    let mut err = square_sum(&res);  // Total residual

    let mut lam = 2.0;  // Lambda, the damping factor
    const NU: f64 = 2.0;     // Nu, the multiplicative increase/decrease factor for lambda
    let mut iter = 0;
    let mut converged_to_local_minimum = false;

    loop {
        calc_jacobian(&p, &mut jac);
        let mut err_trial;
        let mut p_trial;
        let jtj = jac.t().dot(&jac);  // J^T * J
        let jtj_diag = Array2::from_diag(&jtj.diag() );  // diag(J^T * J)
        // Increment/decrease damping until new step gives better result
        loop {
            iter += 1;
            if let Ok(inv) = inv(&jtj + &(lam * &jtj_diag)) {
                p_trial = inv.dot(&jac.t()).dot(&res) + &p;
                calc_residual(&p_trial, &mut res);
                err_trial = square_sum(&res);  // Total residual of trial
                if err_trial < err {
                    break;
                }
                // Converged if step small and error small.
                if (norm(&(&p - &p_trial)) < threshold) && ((err - err_trial).abs() < threshold) {
                    converged_to_local_minimum = true;
                    break;
                }
                if err_trial.is_nan() {
                    return Err(OptimizeError::FunctionReturnedNan(p_trial));
                }
            }
            lam *= NU;
            if iter > iters {
                return Err(OptimizeError::IterationMaximumReached(p));
            }
            if lam.is_infinite() {
                return Err(OptimizeError::GotStuck(p));
            }
        }
        lam /= NU;
        if converged_to_local_minimum {
            break;
        }
        err = err_trial;
        p = p_trial;
    }
    return Ok((p, iter));
}