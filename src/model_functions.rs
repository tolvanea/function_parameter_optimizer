/// This module contains good test functions and data generation methods.

use ndarray as nd;
#[allow(unused_imports)]
use nd::prelude::{Dimension, ShapeBuilder, AsArray, Array1, Array2, Axis, arr1, arr2, Ix1, Ix2};
use ndarray_rand::RandomExt;
use ndarray_rand::rand_distr::{Uniform, Normal};
use peroxide as pr;
use pr::{Real, Number, NumberVector, PowOps, TrigOps};

use crate::misc::square_sum;

const X_MIN: f64 = -3.0;
const X_MAX: f64 = 3.0;
const DATA_LEN: usize = 100;
const STD: f64 = 0.1;

/// Convert auto-differentiable function with `Number`-type to function with
/// f64-type.
// Functional programming, yay!
// https://stackoverflow.com/questions/36390665/how-do-you-pass-a-rust-function-as-a-parameter
pub fn to_f64_fn<F>(f: F) -> Box<dyn Fn(&[f64], &[f64]) -> f64>
where  F: Fn(&[f64], &[Number]) -> Number + 'static
{
    return Box::new(move |r: &[f64], p: &[f64]| {
        let num_vec = Vec::<Number>::from_f64_vec(p.to_vec());
        f(r, num_vec.as_slice()).to_f64()
    })
}

pub fn residual<F>(xgrid: &Array2<f64>, data: &Array1<f64>, f:F, p: &Array1<f64>) -> f64
where F: Fn(&[f64], &[f64]) -> f64
{
    let fit: Array1<f64> = xgrid.outer_iter()
        .map(|x| f(x.as_slice().unwrap(), p.as_slice().unwrap()))
        .collect();
    let err = square_sum(&(&fit-data));
    return err;
}


/// Polynomial
pub mod poly {
    use super::*;
    /// Polynomial function. r: is x-coordinate and p contains coefficients.
    pub fn with_num(r: &[f64], p: &[Number]) -> Number {
        let mut sum: Number = Number::F(0.0);
        for (i, &c) in p.iter().rev().enumerate(){
            sum = sum + c * r[0].powi(i as i32);
        }
        return sum;
    }

    pub fn with_f64(r: &[f64], p: &[f64]) -> f64 {
        return to_f64_fn(with_num)(r,p);
    }

    pub fn grad_p(r: &[f64], p: &[f64]) -> Array1<f64> {
        return (0..p.len()).rev().map(|i| r[0].powi(i as i32)).collect();
    }
    /// Returns x-points, y-values
    pub fn data(p: &[f64]) -> (Array2<f64>, Array1<f64>, f64) {
        let xgrid = nd::Array1::linspace(X_MIN, X_MAX, DATA_LEN);
        let arr = xgrid.mapv(|x| with_f64(&[x], p));
        let noise = Array1::random(DATA_LEN, Normal::new(0., STD).unwrap());
        return (xgrid.insert_axis(Axis(1)), arr+&noise, square_sum(&noise));
    }
}

/// Gaussian
pub mod gaussian {
    use super::*;
    pub fn f<T: Real>(r: &[f64], p: &[T]) -> T {
        use std::f64::consts::PI;
        let (x, mean, std, scale) = (r[0], p[0], p[1], p[2]);
        let f = T::from_f64;
        let scaling = scale / (f(2.0 * PI).sqrt() * std);
        let bell_curve: T = (f(-1.0) * (f(x) - mean).powi(2) / (f(2.0) * std.powi(2))).exp();
        return scaling * bell_curve;
    }
    /// Returns x-points, y-values, and "hidden" real values for parameter
    pub fn data() -> (Array2<f64>, Array1<f64>, Array1<f64>, f64) {
        let xgrid = nd::Array1::linspace(X_MIN, X_MAX, DATA_LEN);
        let p = arr1(&[-1.0, 1.5, 2.0]);    // mean, std, y-scale
        let arr: Array1<f64> = xgrid.mapv(|x| f(&[x], &p.as_slice().unwrap()));
        let noise = Array1::random(DATA_LEN, Normal::new(0.0, STD).unwrap());
        return (xgrid.insert_axis(Axis(1)), arr + &noise, p, square_sum(&noise))
    }
}

/// As a joke, the Rosenbrock's function is incuded. Note, that function is
/// not minimized, rather it's parameters are found out. (Rosenbrock's
/// function is commonly used performance test for optimization algorithms.)
/// This also demostrates that algorithm works on N-dimensional cases.
pub mod rosenbrock {
    use super::*;
    pub fn with_num(r: &[f64], p: &[Number]) -> Number {
        return (p[0]-r[0]).powi(2) + p[1] * (r[1] - r[0].powi(2)).powi(2);
    }
    pub fn with_f64(r: &[f64], p: &[f64]) -> f64 {
        return to_f64_fn(with_num)(r,p);
    }
    /// Returns x-points, y-values, and "hidden" real values for parameter
    pub fn data() -> (Array2<f64>, Array1<f64>, Array1<f64>, f64) {
        let xy_grid = Array2::random((DATA_LEN*20, 2), Uniform::new(-5., 5.));
        let p = arr1(&[1.0, 100.0]);
        let z_values = xy_grid.outer_iter()
            .map(|r| with_f64(r.as_slice().unwrap(), p.as_slice().unwrap())
        ).collect::<Array1<f64>>();
        let noise = Array1::random(xy_grid.dim().0, Normal::new(0., STD).unwrap());
        return (xy_grid, z_values + &noise, p, square_sum(&noise));
    }
}

/// Function that has a lot of local minima and maxima
/// a*cos(bX) + b*sin(aX)
pub mod high_oscillation {
    use super::*;
    pub fn with_num(r: &[f64], p: &[Number]) -> Number {
        return p[0] * (p[1]*r[0]).cos() + p[1]*(p[0]*r[0]).sin();
    }
    pub fn with_f64(r: &[f64], p: &[f64]) -> f64 {
        return to_f64_fn(with_num)(r,p);
    }
    /// Returns x-points, y-values, and "hidden" real values for parameter
    pub fn data(p: &[f64]) -> (Array2<f64>, Array1<f64>, f64) {
        let x_grid = nd::Array1::linspace(X_MIN, X_MAX, DATA_LEN);
        let arr: Array1<f64> = x_grid.iter().map(|&x| with_f64(&[x], p)).collect();
        let noise = (p[0]+p[1]) * Array1::random(DATA_LEN, Normal::new(0., STD).unwrap());
        return (x_grid.insert_axis(Axis(1)), arr + &noise, square_sum(&noise));
    }
}