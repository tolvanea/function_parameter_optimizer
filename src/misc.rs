use ndarray as nd;
#[allow(unused_imports)]  // Copy paste these imports on every file
use nd::{Dimension, ShapeBuilder, AsArray, ScalarOperand,
         ArrayBase, Array, Array1, Array2, ArrayView1, ArrayView2,
         arr1, arr2, Axis, Ix1, Ix2, Data};
use peroxide::{structure::matrix::{Matrix, Shape}, LinearAlgebra, Number, jacobian};
#[allow(unused_imports)]
use anyhow::{Context, Result as Res, anyhow};

/// Construct new array element-wise from two arrays.
///
/// # Example
/// ```
/// use ndarray as nd;
/// use function_parameter_optimizer::misc::apply_fn;
///
/// let a = nd::arr1(&[1.0f64, 2.0, -1.0, -2.0, 3.0]);
/// let b = nd::arr1(&[1,   2,    2,    3,   2]);
/// let o = nd::arr1(&[1.0, 4.0,  1.0, -8.0, 9.0]);
///
/// // This function
/// let out2 = apply_fn(&a, &b, |&a, &b| a.powi(b));
/// #[allow(deprecated)]
/// let close = o.all_close(&out2, 1e-9);
/// assert!(close);
///
/// // Alternatively you can do this by ndarray, which is more cumbersome
/// let mut out1 = a.clone();
/// out1.zip_mut_with(&b, |a, b| *a = (*a).powi(*b));
/// #[allow(deprecated)]
/// let close = o.all_close(&out1, 1e-9);
/// assert!(close);
/// ```
#[allow(dead_code)]
pub fn apply_fn<S1, S2, T1, T2, T3, D, F>(
    first: &ArrayBase<S1, D>,
    second: &ArrayBase<S2, D>,
    f: F,
) -> Array<T3, D>
where
    S1: Data<Elem=T1>, // 1. array of type T1
    S2: Data<Elem=T2>, // 2. array of type T2
    T3: Copy,
    D: ndarray::Dimension,      // Dimension of both arrays
    F: Fn(&T1, &T2) -> T3,      // Function, T3 is out type
{
    assert!(first.shape() == second.shape());
    unsafe {
        let mut out = Array::<T3, D>::uninitialized(first.dim());
        nd::Zip::from(&mut out).and(first).and(second)
            .apply(|a, b, c| *a = f(b, c));
        return out;
    }
}

/// Vector norm of 1d array
pub fn norm<S: Data<Elem=f64>>(a: &ArrayBase<S, Ix1>,) -> f64 {
    a.mapv(|x| x.powi(2)).sum().sqrt()
}
/// Square sum of all the array elements
pub fn square_sum<S: Data<Elem=f64>, D: Dimension>(a: &ArrayBase<S, D>,) -> f64 {
    a.mapv(|x| x.powi(2)).sum()
}
//pub fn norm<'a, A: AsArray<'a, f64>>(a: A) -> f64 {
//    let a = a.into();
//    assert!(a.ndim() == 1);
//    a.mapv(|x| x.powi(2)).sum().sqrt()
//}

pub fn ndarray_to_peroxide(mat: Array2<f64>) -> Matrix {
    let (r, c) = mat.dim();
    let vec = if mat.is_standard_layout() {
        mat.into_raw_vec()
    } else {
        mat.iter().copied().collect()
    };
    return Matrix {
        data: vec,
        row: r,
        col: c,
        shape: Shape::Row,
    };
}

pub fn peroxide_to_ndarray(mat: Matrix) -> Array2<f64> {
    let mat = if mat.shape == Shape::Row {
        mat
    } else {
        // `change_shape` changes fortran array order to c-order
        mat.change_shape()
    };
    return Array2::from_shape_vec((mat.row, mat.col), mat.data).unwrap();
}

/// Matrix inverse
/// Wrapper for ndarray using peroxide-crate inverse
pub fn inv(mat: Array2<f64>) -> Res<Array2<f64>>{
    let dim = mat.dim();
    if dim.0 != dim.1 {
        anyhow!("Matrix must be square.");
    }
    let a = ndarray_to_peroxide(mat);
    let b = a.inv().ok_or(anyhow!("Matrix inverse failed."))?;
    let c = peroxide_to_ndarray(b);
    return Ok(c);
}

/// Moore–Penrose matrix pseudo inverse (A.T * A)^(-1) * A.T
/// Wrapper for ndarray using peroxide-crate inverse
pub fn pseudo_inv(mat: Array2<f64>) -> Res<Array2<f64>>{
    let a = ndarray_to_peroxide(mat);
    let b = a.pseudo_inv().ok_or(anyhow!("Pseudo-inverse failed."))?;
    let c = peroxide_to_ndarray(b);
    return Ok(c);
}


pub fn gradient<F>(f: F, r: &[f64]) -> Array1<f64>
where
    F: Fn(&[Number]) -> Number,
{
    let j1 = jacobian(|r| [f(r.as_slice())].to_vec(), &r.into());
    let j2 = peroxide_to_ndarray(j1);
    assert!(j2.dim().0 == 1);
    return j2.index_axis_move(Axis(0), 0);
}

///// Diagonal of J^T * J
///// This saves computation by calculating only used part
///// # Example
///// ```
///// use ndarray as nd;
///// use function_parameter_optimizer::misc::diagonal_of_jt_j;
///// dbg!("Docktest diagonal_of_jt_j");
///// let j = nd::arr2(&[[1.0, 2.0], [3.0, 4.0], [3.0, 4.0]]);
///// let jt_j_diag1 = diagonal_of_jt_j(&j);
///// let jt_j_diag2 = j.t().dot(&j).diag().to_owned();
///// assert_eq!(jt_j_diag1, jt_j_diag2);
///// ```
//pub fn diagonal_of_jt_j(j: &Array2<f64>) -> Array1<f64>{
//    return j.axis_iter(Axis(1)).map(|row| (&row*&row).sum()).collect();
//}

