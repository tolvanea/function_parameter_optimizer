use ndarray as nd;
#[allow(unused_imports)]
use nd::prelude::{Dimension, ShapeBuilder, AsArray, Array1, Array2, Axis, arr1, arr2, Ix1, Ix2};
use ndarray_rand::RandomExt;
use ndarray_rand::rand_distr::Normal;
use peroxide as pr;
use pr::{Number};

use crate::opmization_algorithms::{gauss_newton, gauss_newton_with_autodiff,
                                   levenberg_marquardt, levenberg_marquardt_with_autodiff,
                                   OptimizeError};
use crate::misc::{pseudo_inv, inv, gradient, norm};
use crate::model_functions::{poly, gaussian, rosenbrock, high_oscillation, residual};

// for datas of the test functions
const X_MIN: f64 = -10.0;
const X_MAX: f64 = 10.0;
const DATA_LEN: usize = 100;
const STD: f64 = 0.01;         // Standard deviation of noisy data
const TOLERANCE: f64 = 0.01;    // Error tolerance for solved parameters in tests


/// This problems should be linear, so solved in one iteration.
/// Write gradient explicitly
#[test]
fn test_linear() {
    fn linear(r: &[f64], p: &[f64]) -> f64 {
        return p[0]* r[0] + p[1];
    }
    // Gradient with respect to parameters
    fn linear_grad_p(r: &[f64], _p: &[f64]) -> Array1<f64> {
        return arr1(&[r[0], 1.0]);
    }
    fn linear_data() -> (Array2<f64>, Array1<f64>, Array1<f64>) {
        let xgrid = nd::Array1::linspace(X_MIN, X_MAX, DATA_LEN);
        let p = arr1(&[0.5, 2.0]);
        let arr = xgrid.mapv(|x| linear(&[x], p.as_slice().unwrap()));
        let noise = Array1::random(DATA_LEN, Normal::new(0., STD).unwrap());
        return (xgrid.insert_axis(Axis(1)), arr+noise, p);
    }

    let (xgrid, data, p_real) = linear_data();
    let p_init = arr1(&[1.0, 1.0]);   // Initial guess for params
    let (p, _) = gauss_newton(linear, linear_grad_p, &xgrid, &data, &p_init, 1000, 1e-9).unwrap();
    assert!(norm(&(&p - &p_real)) < TOLERANCE, "res: {}, answer: {}", &p, &p_real);
}

/// This problems should be linear, so solved in one iteration.
/// Automatic differentiation is used (who-hoo!) along with explicit gradient.
#[test]
fn test_2nd_degree_polynomial_v1() {
    /// second_order_polynomial
    fn poly_2_f64(r: &[f64], p: &[f64]) -> f64 {
        return p[0]*r[0].powi(2) + p[1]*r[0] + p[2];
    }
    fn poly_2_num(r: &[f64], p: &[Number]) -> Number {
        return p[0]*r[0].powi(2) + p[1]*r[0] + p[2];
    }
    // Gradient with respect to parameters
    fn poly_2_grad_p(r: &[f64], _p: &[f64]) -> Array1<f64> {
        return arr1(&[r[0].powi(2), r[0], 1.0]);
    }
    fn poly_2_data() -> (Array2<f64>, Array1<f64>, Array1<f64>) {
        let xgrid = nd::Array1::linspace(X_MIN, X_MAX, DATA_LEN);
        let p = arr1(&[0.1, -2.0, 1.0]);
        let arr = xgrid.mapv(|x| poly_2_f64(&[x], p.as_slice().unwrap()));
        let noise = Array1::random(DATA_LEN, Normal::new(0., STD).unwrap());
        return (xgrid.insert_axis(Axis(1)), arr+noise, p);
    }

    let (xgrid, data, p_real) = poly_2_data();
    let p_init = arr1(&[2.0, 2.0, 2.0]);   // Initial guess for params
    // Test using known gradient
    let (p, _) = gauss_newton(poly_2_f64, poly_2_grad_p, &xgrid, &data, &p_init, 1000, 1e-9).unwrap();
    assert!(norm(&(&p - &p_real)) < TOLERANCE, "res: {}, answer: {}", &p, &p_real);
    // Test using automatic gradient
    let (p, _) = gauss_newton_with_autodiff(poly_2_num, &xgrid, &data, &p_init).unwrap();
    assert!(norm(&(&p - &p_real)) < TOLERANCE, "res: {}, answer: {}", &p, &p_real);
    let (p, _) = levenberg_marquardt(poly_2_f64, poly_2_grad_p, &xgrid, &data, &p_init, 1000, 1e-9).unwrap();
    assert!(norm(&(&p - &p_real)) < TOLERANCE, "res: {}, answer: {}", &p, &p_real);
    // Test using automatic gradient
    let (p, _) = levenberg_marquardt_with_autodiff(poly_2_num, &xgrid, &data, &p_init).unwrap();
    assert!(norm(&(&p - &p_real)) < TOLERANCE, "res: {}, answer: {}", &p, &p_real);
}


/// Test 7th degree polynomial.
/// This is linear problem, but my algorithm solves it somewhy in two steps.
#[test]
fn test_7th_degree_polynomial() {
    let p_real: Array1<f64> = arr1(&[1., -2., 3., -4., 5., -4., 3., -2.]);
    let p_0: Array1<f64> = Array1::from_elem(8, 1.);
    let (xgrid, data, min_err) = poly::data(p_real.as_slice().unwrap());
    let p = match gauss_newton(poly::with_f64, poly::grad_p, &xgrid, &data, &p_0, 1000, 1e-9){
        Ok((p,_)) => p,
        Err(OptimizeError::NonConvexFunction(p)) => p,
        _ => panic!(),
    };
    let err = residual(&xgrid, &data, poly::with_f64, &p);
    assert!(err / min_err < 1.+TOLERANCE, "res: {}, answer: {}", &p, &p_real);
    let (p, _) = gauss_newton_with_autodiff(poly::with_num, &xgrid, &data, &p_0).unwrap();
    let err = residual(&xgrid, &data, poly::with_f64, &p);
    assert!(err / min_err < 1.+TOLERANCE, "res: {}, answer: {}", &p, &p_real);
}

/// I dont understand why, but 9th degree polynomials does not usually work
/// where 7th does. My algorithm complains that 9th polynomial
/// parameter optimization is not convex function, which is just false.
/// Probably some numerical error gets in to the way.
#[test]
fn test_9th_degree_polynomial() {
    let p_real: Array1<f64> = arr1(&[1., -2., 3., -4., 5., -6., 7., -8., 9., 10.]);
    let p_0: Array1<f64> = Array1::from_elem(10, 1.);
    let (xgrid, data, min_err) = poly::data(p_real.as_slice().unwrap());
    let p = match gauss_newton_with_autodiff(poly::with_num, &xgrid, &data, &p_0){
        Ok((p, _)) => p,
        Err(OptimizeError::NonConvexFunction(p)) => p,
        _ => panic!(),
    };
    let err = residual(&xgrid, &data, poly::with_f64, &p);
    assert!(err / min_err < 1.+TOLERANCE, "res: {}, answer: {}", &p, &p_real);
    let (p, _) = gauss_newton_with_autodiff(poly::with_num, &xgrid, &data, &p_0).unwrap();
    let err = residual(&xgrid, &data, poly::with_f64, &p);
    assert!(err / min_err < 1.+TOLERANCE, "res: {}, answer: {}", &p, &p_real);
}

/// Test fitting gaussian curve.
/// This problem should not be linear.
#[test]
fn test_gaussian() {
    let (xgrid, data, p_real, min_err) = gaussian::data();
    let p_init = arr1(&[1.0, 1.0, 1.0]);   // Initial guess for params

    let res_p = gauss_newton_with_autodiff(gaussian::f, &xgrid, &data, &p_init);
    assert!(res_p.is_err());  // Gauss newton does not work here, as problem is not positive definite
    let (p, _) = levenberg_marquardt_with_autodiff(gaussian::f, &xgrid, &data, &p_init).unwrap();
    let err = residual(&xgrid, &data, gaussian::f, &p);
    assert!(err / min_err < 1.+TOLERANCE, "res: {}, answer: {}, err: {}, min_err: {}", &p, &p_real, err, min_err);
}

#[test]
fn test_rosenbrock() {
    let (xgrid, data, p_real, min_err) = rosenbrock::data();
    let p_init = arr1(&[1.0, 1.0]);   // Initial guess for params

    let (p, _) = gauss_newton_with_autodiff(rosenbrock::with_num, &xgrid, &data, &p_init).unwrap();
    let err = residual(&xgrid, &data, rosenbrock::with_f64, &p);
    assert!(err / min_err < 1.+TOLERANCE, "res: {}, answer: {}", &p, &p_real);

    let (p, _) = levenberg_marquardt_with_autodiff(rosenbrock::with_num, &xgrid, &data, &p_init).unwrap();
    let err = residual(&xgrid, &data, rosenbrock::with_f64, &p);
    assert!(err / min_err < 1.+TOLERANCE, "res: {}, answer: {}", &p, &p_real);
}

/// This function is very hard to fit, because there are many local minima
/// a*cos(bX) + b*sin(aX)
/// If a and b are really small, then minimum is found, but else it gets stuck
/// in local minima.
#[test]
fn test_high_oscillation(){
    //                        global min    local min   local min
    for &(coeff, correct) in [(0.01, true), (1., true), (5., false)].iter() {
        let p_real: Array1<f64> = coeff * arr1(&[1.0, 2.0]);
        let mut num_correct = 0;
        for _ in 0..10{
            let (xgrid, data, min_err) = high_oscillation::data(p_real.as_slice().unwrap());
                let p_init = arr1(&[1.0, 1.0]);   // Initial guess for params
            let succes = match levenberg_marquardt_with_autodiff(high_oscillation::with_num, &xgrid, &data, &p_init) {
                Ok((p, _)) => {
                    let err = residual(&xgrid, &data, high_oscillation::with_f64, &p);
                    if err / min_err < 1.+TOLERANCE {1} else {0}
                },
                Err(_) => 0,
            };
            num_correct += succes
        }
        let mean = num_correct as f64 / 10.0;
        assert_eq!(mean > 0.5, correct, "answer: {}, coeff: {}, num_correct: {}", &p_real, coeff, num_correct);
    }
}


#[test]
fn test_automatic_differentiation() {
    let p = arr1(&[0.1, -2.0, 1.0, -1.0]);
    let p_slice = p.as_slice().unwrap();
    let (xgrid, _, _) = poly::data(p.as_slice().unwrap());

    let grad_known = |r: &[f64], p: &[f64]| poly::grad_p(r, p);
    let grad_auto = |r: &[f64], p: &[f64]| gradient(|p| poly::with_num(r, p), p);

    for x in xgrid.outer_iter() {
        let x_slice = x.as_slice().unwrap();
        let known = grad_known(x_slice, p_slice);
        let auto = grad_auto(x_slice, p_slice);
        assert_eq!(known, auto);
        let known = grad_known(x_slice, x_slice);
        let auto = grad_auto(x_slice, x_slice);
        assert_eq!(known, auto);
    }
}

// Test my wrappers for ndarray using peroxide-crate inverse.
#[test]
fn test_inverse_and_pseudo_inverse() {

    fn inv_2x2(m: &Array2<f64>) -> Array2<f64> {
        let (a, b, c, d) = (m[[0,0]], m[[0,1]], m[[1,0]], m[[1,1]]);
        return (1.0 / (a*d - b*c)) * arr2(&[[d, -b ], [-c, a]])
    }

    let a = arr2(&[[1.,2.], [3.,4.], [5.,6.]]);
    let a_pinv_peroxide = pseudo_inv(a.clone()).unwrap();
    let a_pinv_raw = inv_2x2(&a.t().dot(&a)).dot(&a.t());
    #[allow(deprecated)]
    let p = a_pinv_peroxide.all_close(&a_pinv_raw, 1e-12);
    assert!(p);

    let b = arr2(&[[1.,2.], [3.,4.]]);
    let b_inv_peroxide = inv(b.clone()).unwrap();
    let b_inv_raw = inv_2x2(&b);
    #[allow(deprecated)]
    let p = b_inv_peroxide.all_close(&b_inv_raw, 1e-12);
    assert!(p);
}

#[test]
fn test_example() {
    use ndarray::AsArray;

    fn sum<'a, V: AsArray<'a, f64>>(data: V) -> f64 {
        let array_view = data.into();
        array_view.sum()
    }

    assert_eq!(
        sum(&arr1(&[1., 2., 3.])),
        6.
    );
}